package org.jugregator.safestorage;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPublicKey;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class CryptoFunctionsTest {
    private Storage testStorage;

    @Before
    public void setup() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, CertificateException, InvalidKeyException, KeyStoreException, BadPaddingException, IllegalBlockSizeException, NoSuchPaddingException, UnrecoverableEntryException, IOException {
        Context appContext = InstrumentationRegistry.getTargetContext();

        SharedPreferences sharedPrefs = appContext.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        testStorage = new Storage(appContext, sharedPrefs);

        testStorage.createKeys();

        createPassword();
    }


    //@Test
    public void createPassword() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, CertificateException, UnrecoverableEntryException, KeyStoreException, IOException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchPaddingException {
        String password = "sometestpassword";
        RSAPublicKey publicKey = testStorage.readPublicKey();
        testStorage.createAESKey(password, publicKey);
        assertTrue(testStorage.checkPassword(password, publicKey));
    }

    @Test
    public void getAESKey() throws IOException, CertificateException, NoSuchAlgorithmException, InvalidKeyException, UnrecoverableEntryException, NoSuchPaddingException, BadPaddingException, KeyStoreException, IllegalBlockSizeException, NoSuchProviderException, InvalidAlgorithmParameterException {
        createPassword();


        byte[] aesKey = testStorage.getAESSecretKey();

        assertTrue(aesKey.length == 32);
    }

    @Test
    public void encryptFile() throws IOException, CertificateException, NoSuchAlgorithmException, InvalidKeyException, UnrecoverableEntryException, NoSuchPaddingException, BadPaddingException, KeyStoreException, IllegalBlockSizeException, InvalidAlgorithmParameterException, NoSuchProviderException {
        Context appContext = InstrumentationRegistry.getTargetContext();

        Bitmap bitmap = Bitmap.createBitmap(400, 400, Bitmap.Config.RGB_565);

        ByteArrayOutputStream os = new ByteArrayOutputStream();

        assertTrue(bitmap.compress(Bitmap.CompressFormat.PNG, 0, os));

        byte[] pngRaw = os.toByteArray();

        testStorage.encryptBitmap(pngRaw, testStorage.getAESSecretKey(),  new File(appContext.getFilesDir(), "testdata.rawencrypted").getPath());
    }

    @Test
    public void decryptFile() throws IOException, CertificateException, NoSuchAlgorithmException, InvalidKeyException, UnrecoverableEntryException, NoSuchPaddingException, BadPaddingException, KeyStoreException, IllegalBlockSizeException, InvalidAlgorithmParameterException {
        Context appContext = InstrumentationRegistry.getTargetContext();

        Bitmap bitmap = Bitmap.createBitmap(400, 400, Bitmap.Config.RGB_565);


        Paint paint = new Paint();
        paint.setTextSize(100);
        paint.setAntiAlias(true);

        Canvas canvas = new Canvas(bitmap);

        paint.setColor(Color.RED);
        canvas.drawRect(0, 0, 200, 200, paint);
        paint.setColor(Color.WHITE);
        canvas.drawText("Test", 0, 60, paint);

        ByteArrayOutputStream os = new ByteArrayOutputStream();

        assertTrue(bitmap.compress(Bitmap.CompressFormat.PNG, 100, os));

        byte[] pngRaw = os.toByteArray();


        String filename = new File(appContext.getFilesDir(), "testdata.rawencrypted").getPath();
        testStorage.encryptBitmap(pngRaw, testStorage.getAESSecretKey(), filename);


        byte[] pngRawDecrypted = testStorage.decryptImage(testStorage.getAESSecretKey(), filename);

        assertTrue(Arrays.equals(pngRaw, pngRawDecrypted));


        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        Bitmap decodedBitmap = BitmapFactory.decodeByteArray(pngRawDecrypted, 0, pngRawDecrypted.length, options);

        assertTrue(bitmap.sameAs(decodedBitmap));
    }

    @Test
    public void decryptJPEGFile() throws IOException, CertificateException, NoSuchAlgorithmException, InvalidKeyException, UnrecoverableEntryException, NoSuchPaddingException, BadPaddingException, KeyStoreException, IllegalBlockSizeException, InvalidAlgorithmParameterException {
        Context appContext = InstrumentationRegistry.getTargetContext();

        Bitmap bitmap = Bitmap.createBitmap(400, 400, Bitmap.Config.RGB_565);


        Paint paint = new Paint();
        paint.setTextSize(100);
        paint.setAntiAlias(true);

        Canvas canvas = new Canvas(bitmap);

        paint.setColor(Color.RED);
        canvas.drawRect(0, 0, 200, 200, paint);
        paint.setColor(Color.WHITE);
        canvas.drawText("Test", 0, 60, paint);

        ByteArrayOutputStream os = new ByteArrayOutputStream();

        assertTrue(bitmap.compress(Bitmap.CompressFormat.JPEG, 80, os));

        byte[] jpegRaw = os.toByteArray();


        String filename = new File(appContext.getFilesDir(), "testdata.rawencrypted").getPath();
        testStorage.encryptBitmap(jpegRaw, testStorage.getAESSecretKey(), filename);


        byte[] jpegRawDecrypted = testStorage.decryptImage(testStorage.getAESSecretKey(), filename);

        assertTrue(Arrays.equals(jpegRaw, jpegRawDecrypted));


        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        Bitmap decodedBitmap = BitmapFactory.decodeByteArray(jpegRawDecrypted, 0, jpegRawDecrypted.length, options);

        Bitmap decodedSavedJpeg = BitmapFactory.decodeByteArray(jpegRaw, 0, jpegRaw.length, options);
        assertTrue(decodedSavedJpeg.sameAs(decodedBitmap));
    }


    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("org.jugregator.safestorage", appContext.getPackageName());
    }
}
