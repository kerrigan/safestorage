package org.jugregator.safestorage.models;

import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by kerrigan on 13.01.17.
 */

public class Document extends RealmObject {
    @PrimaryKey
    public String id;
    public String title;
    public RealmList<Photo> photos;
    public long date;

    public Document(){
        id = UUID.randomUUID().toString();
        title = "";
        date = System.currentTimeMillis() / 1000;
    }
}
