package org.jugregator.safestorage.models;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by kerrigan on 13.01.17.
 */

public class Photo extends RealmObject {
    @PrimaryKey
    public String id;
    public String filename;

    public Photo(){
        id = UUID.randomUUID().toString();
    }
}
