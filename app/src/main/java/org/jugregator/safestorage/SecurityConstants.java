package org.jugregator.safestorage;

/**
 * Created by kerrigan on 11.01.17.
 */

public class SecurityConstants {
    public static final String KEYSTORE_PROVIDER_ANDROID_KEYSTORE = "AndroidKeyStore";

    public static final String TYPE_RSA = "RSA";
    public static final String TYPE_AES = "AES";

    public static final String TYPE_AES_CBC = "AES/CBC/PKCS5Padding";
    public static final String TYPE_RSA_ECB = "RSA/ECB/NoPadding";
    public static final String TYPE_RSA_NONE = "RSA/NONE/NoPadding";
    public static final String TYPE_DSA = "DSA";
    public static final String TYPE_BKS = "BKS";

    public static final String SIGNATURE_SHA256withRSA = "SHA256withRSA";
    public static final String SIGNATURE_SHA512withRSA = "SHA512withRSA";
}