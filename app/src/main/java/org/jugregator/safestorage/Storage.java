package org.jugregator.safestorage;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.security.KeyPairGeneratorSpec;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.StringJoiner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.x500.X500Principal;

import timber.log.Timber;

/**
 * Created by kerrigan on 11.01.17.
 */

public class Storage {
    private final SharedPreferences preferences;
    private Context context;

    private String mAlias = "key";


    private final static String SALT_KEY = "salt";
    private final static String ENCRYPTED_KEY_KEY = "encryptedKey";
    private final static String DOCUMENT_OPENED_KEY = "documentOpened";


    public Storage(Context context, SharedPreferences preferences){
        this.context = context;
        this.preferences = preferences;
    }

    public void createKeys() throws NoSuchAlgorithmException,
            NoSuchProviderException,
            InvalidAlgorithmParameterException {
        Calendar start = new GregorianCalendar();
        Calendar end = new GregorianCalendar();
        end.add(Calendar.YEAR, 30);

        KeyPairGeneratorSpec spec = new KeyPairGeneratorSpec.Builder(context)
                .setAlias(mAlias)

                .setSubject(new X500Principal("CN=" + mAlias))

                .setSerialNumber(BigInteger.ONE)

                .setStartDate(start.getTime())

                .setEndDate(end.getTime())
                .build();


        KeyPairGenerator kpGenerator = KeyPairGenerator.getInstance(SecurityConstants.TYPE_RSA,
                SecurityConstants.KEYSTORE_PROVIDER_ANDROID_KEYSTORE);
        kpGenerator.initialize(spec);

        kpGenerator.generateKeyPair();
    }

    public KeyStore.PrivateKeyEntry readPrivateKey() throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException, UnrecoverableEntryException {
        KeyStore keyStore = KeyStore.getInstance(SecurityConstants.KEYSTORE_PROVIDER_ANDROID_KEYSTORE);
        keyStore.load(null);
        return (KeyStore.PrivateKeyEntry)keyStore.getEntry(mAlias, null);
    }

    public RSAPublicKey readPublicKey() throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException, UnrecoverableEntryException {
        KeyStore keyStore = KeyStore.getInstance(SecurityConstants.KEYSTORE_PROVIDER_ANDROID_KEYSTORE);
        keyStore.load(null);
        KeyStore.PrivateKeyEntry keyEntry = (KeyStore.PrivateKeyEntry)keyStore.getEntry(mAlias, null);
        return (RSAPublicKey) keyEntry.getCertificate().getPublicKey();
    }


    public void createAESKey(String password, RSAPublicKey publicKey) throws NoSuchAlgorithmException, UnsupportedEncodingException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        String deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        MessageDigest digest = MessageDigest.getInstance("SHA-256");

        digest.update(deviceId.getBytes());

        byte[] deviceIdHash = digest.digest();

        digest.reset();
        digest.update(password.getBytes("UTF-8"));
        byte[] passwordHash = digest.digest();
        digest.reset();


        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");

        byte[] SALT = new byte[8];
        random.nextBytes(SALT);



        byte[] preKey = new byte[SALT.length + deviceIdHash.length + passwordHash.length];

        System.arraycopy(SALT, 0, preKey, 0, SALT.length);
        System.arraycopy(deviceIdHash, 0, preKey, SALT.length, deviceIdHash.length);
        System.arraycopy(passwordHash, 0, preKey, SALT.length + deviceIdHash.length, passwordHash.length);

        digest.update(preKey);

        byte[] aesKey = digest.digest();

        digest.reset();

        Cipher cipher = Cipher.getInstance(SecurityConstants.TYPE_RSA_NONE);
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);

        byte[] encryptedKey = cipher.doFinal(aesKey);

        preferences.edit()
                .putString(SALT_KEY, Base64.encodeToString(SALT, Base64.DEFAULT))
                .putString(ENCRYPTED_KEY_KEY, Base64.encodeToString(encryptedKey, Base64.DEFAULT))
                .apply();
    }

    public boolean checkPassword(String password, RSAPublicKey publicKey) throws NoSuchAlgorithmException, IOException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        String deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        MessageDigest digest = MessageDigest.getInstance("SHA-256");

        digest.update(deviceId.getBytes());

        byte[] deviceIdHash = digest.digest();

        digest.reset();
        digest.update(password.getBytes("UTF-8"));
        byte[] passwordHash = digest.digest();
        digest.reset();

        byte[] SALT;
        String saltBase64 = preferences.getString(SALT_KEY, "");

        if (saltBase64.length() == 0){
            throw new IOException("Failed to read salt");
        }


        SALT = Base64.decode(saltBase64, Base64.DEFAULT);


        String encryptedKeyBase64 = preferences.getString(ENCRYPTED_KEY_KEY, "");
        if (encryptedKeyBase64.length() == 0){
            throw new IOException("Failed to read encrypted key");
        }

        byte[] storedEncryptedKey = Base64.decode(encryptedKeyBase64, Base64.DEFAULT);

        byte[] preKey = new byte[SALT.length + deviceIdHash.length + passwordHash.length];

        System.arraycopy(SALT, 0, preKey, 0, SALT.length);
        System.arraycopy(deviceIdHash, 0, preKey, SALT.length, deviceIdHash.length);
        System.arraycopy(passwordHash, 0, preKey, SALT.length + deviceIdHash.length, passwordHash.length);

        digest.update(preKey);

        byte[] aesKey = digest.digest();

        digest.reset();

        Cipher cipher = Cipher.getInstance(SecurityConstants.TYPE_RSA);
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);

        byte[] encryptedKey = cipher.doFinal(aesKey);

        return Arrays.equals(encryptedKey, storedEncryptedKey);

    }

    public byte[] getAESSecretKey() throws CertificateException, UnrecoverableEntryException, NoSuchAlgorithmException, KeyStoreException, IOException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        KeyStore.PrivateKeyEntry privateKeyEntry = readPrivateKey();

        Cipher cipher = Cipher.getInstance(SecurityConstants.TYPE_RSA_NONE);
        cipher.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());

        String encryptedKeyBase64 = preferences.getString(ENCRYPTED_KEY_KEY, "");
        if (encryptedKeyBase64.length() == 0){
            throw new IOException("Failed to read encrypted key");
        }

        byte[] storedEncryptedKey = Base64.decode(encryptedKeyBase64, Base64.DEFAULT);

        byte[] decrypted = cipher.doFinal(storedEncryptedKey);

        byte[] aesKey = new byte[32];

        System.arraycopy(decrypted, 224, aesKey, 0, 256 / 8);

        return aesKey;
    }


    public void encryptBitmap(byte[] bitmapRawData, byte[] secretKey, String filename) throws IOException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, InvalidAlgorithmParameterException {
        FileOutputStream fos = new FileOutputStream(filename);

        SecretKeySpec sks = new SecretKeySpec(secretKey, SecurityConstants.TYPE_AES);

        //Init IV
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        byte[] IV = new byte[16];
        random.nextBytes(IV);

        Cipher cipher = Cipher.getInstance(SecurityConstants.TYPE_AES_CBC);
        cipher.init(Cipher.ENCRYPT_MODE, sks, new IvParameterSpec(IV));


        fos.write(IV);
        fos.flush();
        CipherOutputStream cos = new CipherOutputStream(fos, cipher);

        cos.write(bitmapRawData);
        cos.flush();
        cos.close();
    }


    public byte[] decryptImage(byte[] secretKey, String filename) throws IOException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException {
        FileInputStream fis = new FileInputStream(filename);
        SecretKeySpec sks = new SecretKeySpec(secretKey, SecurityConstants.TYPE_AES);
        Cipher cipher = Cipher.getInstance(SecurityConstants.TYPE_AES_CBC);

        byte[] IV = new byte[16];

        int bytesRead = fis.read(IV);
        if (bytesRead < IV.length){
            throw new IOException("File is too small and doesn't contains IV");
        }

        cipher.init(Cipher.DECRYPT_MODE, sks, new IvParameterSpec(IV));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        CipherInputStream cis = new CipherInputStream(fis, cipher);

        int b;
        byte[] d = new byte[16];

        int counter = 0;

        while ((b = cis.read(d)) != -1){
            counter++;
            baos.write(d, 0, b);
        }

        cis.close();


        byte[] bitmapRawData = baos.toByteArray();

        baos.close();

        return bitmapRawData;
    }

    public boolean isRegistered(){
        return preferences.contains(ENCRYPTED_KEY_KEY);
    }

    public void removeCredentials(){
        preferences.edit()
                .clear()
                .apply();
        try {
            KeyStore keyStore = KeyStore.getInstance(SecurityConstants.KEYSTORE_PROVIDER_ANDROID_KEYSTORE);
            keyStore.load(null);
            keyStore.deleteEntry(mAlias);
        } catch (KeyStoreException | CertificateException | IOException | NoSuchAlgorithmException e) {
            Timber.w("Failed to remove RSA pair from hardware keystore");
        }
    }

    public boolean getDocumentOpened(){
        return preferences.getBoolean(DOCUMENT_OPENED_KEY, false);
    }

    public void setDocumentOpened(boolean opened){
        preferences.edit()
                .putBoolean(DOCUMENT_OPENED_KEY, opened)
                .apply();
    }
}
