package org.jugregator.safestorage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.util.LruCache;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import org.jugregator.safestorage.models.Photo;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by kerrigan on 13.01.17.
 */

public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.PhotoViewHolder> {

    private final Context context;
    private final LruCache<String, Bitmap> cache;
    private List<Photo> items;
    private final Storage storage;
    private int selectedIndex;

    public DocumentAdapter(Context context){
        this.context = context;
        this.storage = new Storage(context, context.getSharedPreferences("preferences", Context.MODE_PRIVATE));
        this.cache = new LruCache<>(10);
    }

    public void setItems(List<Photo> items){
        this.items = items;
        selectedIndex = -1;
        notifyDataSetChanged();
    }

    public Photo getItem(int position){
        if (position > getItemCount() - 1){
            return null;
        }

        return items.get(position);
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.photo, parent, false);
        return new PhotoViewHolder(v, this);
    }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public void setSelected(int selected) {
        this.selectedIndex = selected;
        notifyDataSetChanged();
    }

    public int getSelected() {
        return selectedIndex;
    }

    Bitmap getDecryptedBitmap(String filename){
        Bitmap bitmapFromCache = cache.get(filename);
        if (bitmapFromCache != null){
            return bitmapFromCache;
        }

        try {
            byte[] rawData = storage.decryptImage(storage.getAESSecretKey(), filename);

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;


            Bitmap decryptedBitmap = BitmapFactory.decodeByteArray(rawData, 0, rawData.length, options);

            int originWidth = options.outWidth;
                    int originHeight = options.outHeight;


            if(originWidth * originHeight >= 3 * 1000 * 1000){
                int newWidth = originWidth;
                int newHeight = originHeight;


                int inSampleSize = 1;
                while (newWidth * newHeight >= 3 * 1000 * 1000){
                    newWidth /= 2;
                    newHeight /= 2;
                    inSampleSize++;
                }

                options.inJustDecodeBounds = false;
                options.inSampleSize = inSampleSize;


                decryptedBitmap = BitmapFactory.decodeByteArray(rawData, 0, rawData.length, options);
            } else {
                options.inJustDecodeBounds = false;
                decryptedBitmap = BitmapFactory.decodeByteArray(rawData, 0, rawData.length, options);
            }

            cache.put(filename, decryptedBitmap);

            return decryptedBitmap;
        } catch (IOException | NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | InvalidAlgorithmParameterException | UnrecoverableEntryException | CertificateException | BadPaddingException | KeyStoreException | IllegalBlockSizeException e) {
            Timber.d("Failed to decrypt photo");
            return null;
        }
    }

    public static class PhotoViewHolder extends RecyclerView.ViewHolder {

        private final DocumentAdapter adapter;

        @BindView(R.id.ivPhoto)
        ImageView ivPhoto;

        public PhotoViewHolder(View itemView, DocumentAdapter adapter) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.adapter = adapter;
        }

        public void bind(Photo photo){

            Bitmap image = adapter.getDecryptedBitmap(photo.filename);

            // TODO: 14.01.17 here must be memory cache and photo resize
            if (image != null){
                ivPhoto.setImageBitmap(image);
            }

            if (getAdapterPosition() == adapter.getSelected()){
                itemView.setBackgroundColor(Color.GREEN);
            } else {
                itemView.setBackgroundColor(Color.WHITE);
            }

            ivPhoto.setOnLongClickListener(v -> {
                adapter.setSelected(getAdapterPosition());
                return true;
            });

            // TODO: 13.01.17 decrypt photo and show
        }
    }
}
