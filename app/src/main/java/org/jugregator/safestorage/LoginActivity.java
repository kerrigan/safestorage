package org.jugregator.safestorage;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.jugregator.safestorage.models.Photo;

import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;
import timber.log.Timber;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.etPassword)
    EditText etPassword;

    @BindView(R.id.btnClearAllData)
    Button btnClearAllData;

    private Storage storage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        SharedPreferences preferences = getSharedPreferences("preferences", MODE_PRIVATE);
        storage = new Storage(this, preferences);

        ButterKnife.bind(this);

        if (storage.isRegistered()){
            etPassword.setHint(R.string.enter_password);
            btnClearAllData.setEnabled(true);
        } else {
            etPassword.setHint(R.string.create_password);
            btnClearAllData.setEnabled(false);
        }

        btnClearAllData.setOnClickListener(v -> onClearAllDataClicked());
        etPassword.setOnEditorActionListener((TextView textView, int actionId, KeyEvent keyEvent) -> onPasswordAction(actionId));
    }

    private void onClearAllDataClicked() {
        Realm realm = Realm.getDefaultInstance();

        RealmResults<Photo> photos = realm.where(Photo.class).findAll();
        for (Photo photo: photos) {
            if (new File(photo.filename).delete()){
                Timber.w("Failed to remove encrypted file %s", photo.filename);
            }
        }

        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
        storage.removeCredentials();

        Toast.makeText(this, R.string.all_data_removed, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private boolean onPasswordAction(int actionId) {
        if (actionId == EditorInfo.IME_ACTION_SEND) {
            try {
                if (storage.isRegistered()) {
                    if (storage.checkPassword(etPassword.getText().toString(), storage.readPublicKey())) {
                        startActivity(new Intent(this, DocumentListActivity.class));
                        finish();
                    } else {
                        Toast.makeText(this, R.string.password_is_wrong, Toast.LENGTH_LONG).show();
                        etPassword.setText("");
                        return true;
                    }
                } else {
                    storage.createKeys();
                    storage.createAESKey(etPassword.getText().toString(), storage.readPublicKey());
                    startActivity(new Intent(this, DocumentListActivity.class));
                    finish();

                }
            } catch (NoSuchAlgorithmException | NoSuchProviderException | UnrecoverableEntryException | BadPaddingException | KeyStoreException | InvalidKeyException | CertificateException | NoSuchPaddingException | IOException | IllegalBlockSizeException | InvalidAlgorithmParameterException e) {
                Timber.w("Failed to login/create password");
            }
            Timber.d("onCreate: password entered");
        }
        return true;
    }
}
