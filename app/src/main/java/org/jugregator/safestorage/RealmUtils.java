package org.jugregator.safestorage;

import io.realm.Realm;

/**
 * Created by kerrigan on 13.01.17.
 */

public class RealmUtils {
    public static Realm getRealm(){
        return Realm.getDefaultInstance();
    }
}
