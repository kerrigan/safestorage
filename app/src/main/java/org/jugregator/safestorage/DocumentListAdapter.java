package org.jugregator.safestorage;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.jugregator.safestorage.models.Document;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kerrigan on 13.01.17.
 */

public class DocumentListAdapter extends RecyclerView.Adapter<DocumentListAdapter.DocumentViewHolder> {


    private final Context context;
    private final Storage storage;
    private List<Document> items = null;

    private int selectedIndex = -1;

    public DocumentListAdapter(Context context, Storage storage){
        this.context = context;
        this.storage = storage;
    }

    public void setItems(List<Document> items){
        this.items = items;
        selectedIndex = -1;
        notifyDataSetChanged();
    }

    @Override
    public DocumentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.document, parent, false);
        return new DocumentViewHolder(v, this);
    }

    @Override
    public void onBindViewHolder(DocumentViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public Document getItem(int position){
        if (position > getItemCount() - 1){
            return null;
        }

        return items.get(position);
    }


    public void setSelected(int selected) {
        this.selectedIndex = selected;
        notifyDataSetChanged();
    }

    public int getSelected() {
        return selectedIndex;
    }


    Storage getStorage(){
        return this.storage;
    }


    public static class DocumentViewHolder extends RecyclerView.ViewHolder {

        private final DocumentListAdapter adapter;

        @BindView(R.id.txtTitle)
        TextView txtTitle;

        public DocumentViewHolder(View itemView, DocumentListAdapter adapter) {
            super(itemView);
            this.adapter = adapter;
            ButterKnife.bind(this, itemView);
        }

        public void bind(Document document) {
            txtTitle.setText(document.title);

            if (adapter.getSelected() == getAdapterPosition()){
                txtTitle.setBackgroundColor(Color.GREEN);
            } else {
                txtTitle.setBackgroundColor(Color.WHITE);
            }

            txtTitle.setOnLongClickListener(view -> {
                adapter.setSelected(getAdapterPosition());
                return true;
            });

            txtTitle.setOnClickListener(view -> {
                //view part
                adapter.getStorage().setDocumentOpened(true);
                Context context = view.getContext();
                Intent intent = new Intent(context, DocumentActivity.class);
                intent.putExtra(DocumentActivity.DOCUMENT_ID_ID, document.id);
                context.startActivity(intent);
            });
        }
    }
}
