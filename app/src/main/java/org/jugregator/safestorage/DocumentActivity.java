package org.jugregator.safestorage;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.Toast;

import org.jugregator.safestorage.models.Document;
import org.jugregator.safestorage.models.Photo;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.UUID;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;
import timber.log.Timber;

public class DocumentActivity extends AppCompatActivity {
    public static final String DOCUMENT_ID_ID = "documentId";
    private static final int REQUEST_TAKE_PHOTO = 1;

    @BindView(R.id.rvPhotos)
    RecyclerView mRvPhotos;

    @BindView(R.id.ibAdd)
    ImageButton mIbAdd;

    @BindView(R.id.ibRemove)
    ImageButton mIbRemove;

    private DocumentAdapter mAdapter;
    private Realm realm;

    //TODO: move to model
    private static String mCurrentPhotoPath;
    private Storage storage;
    private Document document;
    private boolean photoCreated = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Prevent screenshots
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        setContentView(R.layout.activity_document);
        ButterKnife.bind(this);

        String documentId = getIntent().getStringExtra(DOCUMENT_ID_ID);


        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,
                layoutManager.getOrientation());
        mRvPhotos.addItemDecoration(dividerItemDecoration);
        mRvPhotos.setItemAnimator(new DefaultItemAnimator());
        mRvPhotos.setLayoutManager(layoutManager);

        mAdapter = new DocumentAdapter(this);

        mIbAdd.setOnClickListener(v -> onAddCLicked());

        mIbRemove.setOnClickListener(v -> onRemoveClicked());



        storage = new Storage(this, getSharedPreferences("preferences", Context.MODE_PRIVATE));

        realm = Realm.getDefaultInstance();

        RealmResults<Document> result = realm.where(Document.class).equalTo("id", documentId).findAll();

        if (result.size() == 0){
            finish();
            return;
        }


        document = result.get(0);
        setTitle(document.title);
        mAdapter.setItems(document.photos);
        mRvPhotos.setAdapter(mAdapter);


        result.addChangeListener(items -> {
            if (items.size() == 0){
                return;
            }
            mAdapter.setItems(items.get(0).photos);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        photoCreated = false;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (!photoCreated) {
            Timber.i("Document reopened, proceed to login");
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }


    private void onRemoveClicked() {
        // remove photo and files
        int selectedIndex = mAdapter.getSelected();
        if (selectedIndex != -1){
            Photo photo = mAdapter.getItem(selectedIndex);
            if (!new File(photo.filename).delete()){
                Timber.w("Failed to remove encrypted file");
            }

            realm.beginTransaction();
            photo.deleteFromRealm();
            realm.commitTransaction();

            mAdapter.notifyItemRemoved(selectedIndex);
        }
    }

    private void onAddCLicked() {
        // create photo

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            try {
                File photoFile = createImageFile();


                Uri photoURI = FileProvider.getUriForFile(this,
                        "org.jugregator.safestorage.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(this, "Can't create temporary file", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private File createImageFile() throws IOException {
        String imageFileName = "unencrypted";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK){
            // get taken photo from mCurrentPhotoPath, put to realm, encrypt with aes
            photoCreated = true;

            File temporaryFile = new File(mCurrentPhotoPath);
            // TODO: 14.01.17 Add resize to prevent OOM
            ByteArrayOutputStream baos = new ByteArrayOutputStream();


            try {
                FileInputStream fis = new FileInputStream(temporaryFile);
                int b;
                byte[] d = new byte[16];
                while ((b = fis.read(d)) != -1){
                    baos.write(d, 0, b);
                }

                byte[] rawData = baos.toByteArray();

                String encryptedFileName =  new File(getFilesDir(), UUID.randomUUID().toString() + ".encrypted").getPath();
                storage.encryptBitmap(rawData, storage.getAESSecretKey(), encryptedFileName);


                FileOutputStream rewriteFos = new FileOutputStream(temporaryFile);
                //rewrite photo with zeros
                for (int i = 0; i < temporaryFile.length(); i++) {
                    rewriteFos.write(0);
                }
                rewriteFos.close();

                //remove original photo
                if (!temporaryFile.delete()) {
                    Timber.d("onActivityResult: Failed to remove temporary file");
                }

                // TODO: 14.01.17 move this to model
                realm.beginTransaction();
                Photo photo = new Photo();
                photo.filename = encryptedFileName;
                document.photos.add(photo);
                realm.insertOrUpdate(document);
                realm.commitTransaction();
                // END
            } catch (IOException e){
                Timber.w("!!!Failed to rewrite and delete temporary file!!!");
            } catch (UnrecoverableEntryException | NoSuchAlgorithmException | CertificateException | KeyStoreException | InvalidKeyException | InvalidAlgorithmParameterException | BadPaddingException | NoSuchPaddingException | IllegalBlockSizeException e) {
                Toast.makeText(this, R.string.failed_to_encrypt_file, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        setResult(RESULT_OK);
        finish();
    }
}
