package org.jugregator.safestorage;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by kerrigan on 13.01.17.
 */

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        DebugUtils.initDebug();
    }
}