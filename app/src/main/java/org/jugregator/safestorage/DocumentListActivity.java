package org.jugregator.safestorage;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import org.jugregator.safestorage.models.Document;
import org.jugregator.safestorage.models.Photo;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;
import timber.log.Timber;

public class DocumentListActivity extends AppCompatActivity {

    @BindView(R.id.rvDocuments)
    RecyclerView mRvDocuments;

    @BindView(R.id.ibAdd)
    ImageButton mIbAdd;

    @BindView(R.id.ibEdit)
    ImageButton mIbEdit;

    @BindView(R.id.ibRemove)
    ImageButton mIbRemove;

    private DocumentListAdapter mAdapter;
    private Realm realm;
    private Storage storage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Prevent screenshots
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_document_list);


        ButterKnife.bind(this);


        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRvDocuments.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,
                layoutManager.getOrientation());
        mRvDocuments.addItemDecoration(dividerItemDecoration);
        mRvDocuments.setItemAnimator(new DefaultItemAnimator());


        storage = new Storage(this, getSharedPreferences("preferences", MODE_PRIVATE));
        mAdapter = new DocumentListAdapter(this, storage);

        realm = RealmUtils.getRealm();

        RealmResults<Document> items = realm.where(Document.class).findAllSorted("date");

        mAdapter.setItems(items);

        items.addChangeListener(results -> mAdapter.setItems(results));

        mRvDocuments.setAdapter(mAdapter);

        mIbAdd.setOnClickListener(view -> onAddClicked());
        mIbRemove.setOnClickListener(view -> onRemoveClicked());
        mIbEdit.setOnClickListener(view -> onEditClicked());
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        if (!storage.getDocumentOpened()) {
            Timber.i("Document list reopened, proceed to login");
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
        storage.setDocumentOpened(false);
    }

    private void onRemoveClicked() {
        int selected = mAdapter.getSelected();
        if (selected != -1){
            Document itemToDelete = mAdapter.getItem(selected);


            for (Photo photo: itemToDelete.photos) {
                if (! new File(photo.filename).delete()){
                    Timber.w("Failed to remove encrypted file %s", photo.filename);
                }
            }
            realm.beginTransaction();
            itemToDelete.deleteFromRealm();
            realm.commitTransaction();

            mAdapter.notifyItemRemoved(selected);
            // TODO: 15.01.2017 remove files
        }
    }

    private void onEditClicked() {
        int selected = mAdapter.getSelected();
        if (selected != -1){
            Document itemToEdit = mAdapter.getItem(selected);


            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.edit_document_title);

            final EditText input = new EditText(this);
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            builder.setView(input);

            builder.setPositiveButton("OK", (dialog, which) -> {
                if (input.getText().length() == 0){
                    Toast.makeText(DocumentListActivity.this, R.string.document_title_is_empty, Toast.LENGTH_SHORT).show();
                    return;
                }

                String title = input.getText().toString();

                //Move to presenter

                realm.beginTransaction();
                itemToEdit.title = title;
                realm.insertOrUpdate(itemToEdit);
                realm.commitTransaction();
            });

            builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());

            builder.show();
        }
    }

    private void onAddClicked() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.enter_document_title);

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton(R.string.ok, (dialog, which) -> {
            if (input.getText().length() == 0){
                Toast.makeText(DocumentListActivity.this, R.string.document_title_is_empty, Toast.LENGTH_SHORT).show();
                return;
            }

            String title = input.getText().toString();

            //Move to presenter
            Document document = new Document();
            document.title = title;

            realm.beginTransaction();
            realm.insertOrUpdate(document);
            realm.commitTransaction();
        });

        builder.setNegativeButton(R.string.cancel, (dialog, which) -> dialog.cancel());
        builder.show();
    }
}
