package org.jugregator.safestorage;

import timber.log.Timber;

public class DebugUtils {
    public static void initDebug(){
        Timber.plant(new Timber.DebugTree());
    }
}